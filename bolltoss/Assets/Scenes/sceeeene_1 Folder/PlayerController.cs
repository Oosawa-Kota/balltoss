﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    Rigidbody2D rigid2D;
    bool orShift = false;
    float jumpForce = 375.0f;//ジャンプ力
    float walkForce = 15.0f;//
    float maxWalkSpeed = 20.0f;//歩くスピードの制限値
    bool grandTathi = false;


    void Start()
    {
        rigid2D = GetComponent<Rigidbody2D>();//このコンポーネントを読み込む
    }

    void Update()
    {
        int key = 0;


        //左右移動
        if (orShift == false)
        {
            if (grandTathi == true)
            {
                //ジャンプする
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    rigid2D.AddForce(transform.up * this.jumpForce);//
                }
            }
            if (Input.GetKey(KeyCode.RightArrow)) key = 1;
            if (Input.GetKey(KeyCode.LeftArrow)) key = -1;
            //歩く方向に応じて反転
            if (key != 0)
            {
                transform.localScale = new Vector3(key, 1, 1);
            }
        }
        //プレイヤの速度
        float speedx = Mathf.Abs(this.rigid2D.velocity.x);

        //スピード制限
        if (speedx < maxWalkSpeed)//スピード制限
        {
            this.rigid2D.AddForce(transform.right * key * this.walkForce);
        }


        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            orShift = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            orShift = false;
            key = 0;
        }
    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        grandTathi = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        grandTathi = false;
    }
}