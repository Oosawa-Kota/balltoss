﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2Respown : MonoBehaviour
{
    public GameObject CatPrefab;
    float span = 3.0f;
    float delta = 0;
    bool catResFlag = false;

    // Start is called before the first frame update
    void Start()
    {
        GameObject spown = Instantiate(CatPrefab) as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(catResFlag == true)
        {
            this.delta += Time.deltaTime;
            if (this.delta > this.span)
            {
                delta = 0;
                GameObject spown = Instantiate(CatPrefab) as GameObject;
                spown.transform.position = transform.position;
                catResFlag = false;
            }
        }
    }
    public void CRF()
    {
        catResFlag = true;
    }
}
