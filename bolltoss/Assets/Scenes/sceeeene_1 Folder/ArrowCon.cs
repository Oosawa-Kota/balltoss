﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowCon : MonoBehaviour
{
    bool onShift = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float key = 0;

        if (Input.GetKey(KeyCode.RightArrow)) key = 0.4f;
        if (Input.GetKey(KeyCode.LeftArrow)) key = -0.4f;
        if (key != 0)
        {
            transform.localScale = new Vector3(key, 0.4f, 0.4f);
        }

        if (onShift==true)
        {

            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Rotate(0, 0, -2.0f);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Rotate(0, 0, 2.0f);
            }

        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            onShift=true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            onShift = false;
        }
    }
}
