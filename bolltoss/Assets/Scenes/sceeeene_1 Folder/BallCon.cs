﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallCon : MonoBehaviour
{
    private GameObject lastTouchLocation;
    float jumpForce = 500.0f;
    Rigidbody2D rigid2D;
    int flag = 0;
    // Start is called before the first frame update
    void Start()
    {
        rigid2D = GetComponent<Rigidbody2D>();
        rigid2D.AddForce(transform.up * this.jumpForce);
    }

    // Update is called once per frame
    void Update()
    {
        if (flag==1)
        {

        }
        if (flag == 2)
        {
            //スコア処理を追加
            FindObjectOfType<Score>().AddPoint(1);

            //相手のタグがnameならば、自分を消す
            Destroy(gameObject);
           
        }

        if (transform.position.y < -5.0f)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //衝突判定
        if (collision.gameObject.tag == "Basket2Prefab")
        {
            flag = 2;
        }
        else if(collision.gameObject.tag == "Basket")
        {
            flag = 2;
        }
        else
        {
            flag = 1;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        flag = 0;
    }
 
    
}

