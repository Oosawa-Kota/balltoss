﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Flag_Gen : MonoBehaviour
{
    public GameObject Flag;
    private int shotPoint;
    bool on = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
   
        if (on==true)
        {

            GameObject flag = Instantiate(Flag) as GameObject;
            flag.transform.position = new Vector3(2, 8, 0);
            on = false;

        }
    }
    //ポイントの追加
    public void AddPoint(int point)
    {
        shotPoint = shotPoint + point;
        if (shotPoint == 5)
        {
            on = true;
        }
    }
}
