﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public GameObject stage_1_Pre;
    public GameObject Stage;
    public GameObject casoru;
    GameObject Dog_point;
    GameObject Cat_point;
    GameObject Txst;
    float delta = 0;
    int Minute = 1;
    bool timecount = true;
    public static int dogcount = 0;
    public static int catcount = 0;
    public Text text;
    GameObject Result;


    public GameObject ResultCanvas;

    public static int player = 0;


    // Start is called before the first frame update
    void Start()
    {
        GameObject Result = GameObject.Find("ResultCanvas");
        
        TimeCountOn();
        GameObject Bullet = Instantiate(Stage) as GameObject;
        Dog_point = GameObject.Find("Dog_Point");
        Cat_point = GameObject.Find("Cat_Point");
        Txst = GameObject.Find("Text");
    }

    // Update is called once per frame
    void Update()
    {
        if (timecount == true)
        {
            this.delta -= Time.deltaTime;
            this.Txst.GetComponent<Text>().text = Minute + (".") + delta.ToString("F2");
            if (delta < 0 && Minute == 0)
            {
                this.TimeCountOff();
                SceneManager.LoadScene("endResult");
            }
            if (delta < 0)
            {
                Minute--;
                delta = 60;
            }

            if (dogcount == 5)
            {
                this.TimeCountOff();
                SceneManager.LoadScene("endResult");
            }
            if (catcount == 5)
            {
                this.TimeCountOff();
                SceneManager.LoadScene("endResult");
            }
        }
    }
    public void P1Point_()
    {
        Dog_point.GetComponent<Image>().fillAmount -= 1.0f / 5;
        dogcount++;
    }
    public void P2Point_()
    {
        Cat_point.GetComponent<Image>().fillAmount -= 1.0f / 5;
        catcount++;
    }
    public static int WinPlayer()
    {
        if (dogcount < catcount)
        {
            player = 0;
        }else if(dogcount > catcount){
            player = 1;
        }else{
            player = 2;
        }


        return player;
    }
    public static int P1_Result()
    {
        return catcount;
    }
    public static int P2_Result()
    {
        return dogcount;
    }
    private void TimeCountOff()
    {
        timecount = false;
        //GameObject Bullet = Instantiate(ResultCanvas) as GameObject;
        //GameObject asfawfa = Instantiate(casoru) as GameObject;
    }
    public void TimeCountOn()
    {
        timecount = true;
        
        Destroy(Result);
    }
    public static void CountReset()
    {
        dogcount = 0;
        catcount = 0;
    }

}
