﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    GameObject Timer;
    float span = 1.0f;
    float delta = 0;
    int time = 0;
    // Start is called before the first frame update
    void Start()
    {
        Timer = GameObject.Find("Timer");
    }
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            time += 1;


            Timer.GetComponent<Image>().fillAmount -= 1.0f/60;
            //if (time == 61) Scene.Management.LoadScene("SampleScene");
        }
    }

}
