﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGen : MonoBehaviour
{
    public GameObject BallPrefab;
    bool onShift = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (onShift == true)
        {

            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject Ball = Instantiate(BallPrefab) as GameObject;
                Ball.transform.position = transform.position;
                Ball.transform.rotation = transform.rotation;
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftShift)) onShift = true;
        if (Input.GetKeyUp(KeyCode.LeftShift)) onShift = false;
    }
    
}
