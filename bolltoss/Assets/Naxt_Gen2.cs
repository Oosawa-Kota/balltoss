﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Naxt_Gen2 : MonoBehaviour
{
    public GameObject WallPrefab;
    private int shotPoint;
    Vector3[] order = new Vector3[6];
    int ord = 0;
    bool flag = true;


    // Start is called before the first frame update
    void Start()
    {
        order[0] = new Vector3(2, -3.5f, 0);
        order[1] = new Vector3(0, -1, 0);
        order[2] = new Vector3(-2, 1, 0);
        order[3] = new Vector3(2, 3, 0);
        order[4] = new Vector3(4, 5, 0);
        order[5] = new Vector3(2, 7, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (flag == true)
        {
            GameObject Wall = Instantiate(WallPrefab) as GameObject;
            Wall.transform.position = order[ord];
            flag = false;
        }

    }



    //ポイントの追加
    public void AddPoint(int point)
    {
        shotPoint = shotPoint + point;

    }
    public void wallnext()
    {
        if (ord < 5)
        {
            ord++;
            flag = true;
        }

    }
}
